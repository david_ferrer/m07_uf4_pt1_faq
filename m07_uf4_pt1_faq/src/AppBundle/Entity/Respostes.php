<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Respostes
 *
 * @ORM\Table(name="respostes", indexes={@ORM\Index(name="id_pregunta", columns={"id_pregunta"})})
 * @ORM\Entity
 */
class Respostes
{
    /**
     * @var string
     *
     * @ORM\Column(name="text_resposta", type="string", length=255, nullable=true)
     */
    private $textResposta;

    /**
     * @var integer
     *
     * @ORM\Column(name="correcta", type="integer", nullable=true)
     */
    private $correcta;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Preguntes
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Preguntes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pregunta", referencedColumnName="id")
     * })
     */
    private $idPregunta;



    /**
     * Set textResposta
     *
     * @param string $textResposta
     *
     * @return Respostes
     */
    public function setTextResposta($textResposta)
    {
        $this->textResposta = $textResposta;

        return $this;
    }

    /**
     * Get textResposta
     *
     * @return string
     */
    public function getTextResposta()
    {
        return $this->textResposta;
    }

    /**
     * Set correcta
     *
     * @param integer $correcta
     *
     * @return Respostes
     */
    public function setCorrecta($correcta)
    {
        $this->correcta = $correcta;

        return $this;
    }

    /**
     * Get correcta
     *
     * @return integer
     */
    public function getCorrecta()
    {
        return $this->correcta;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPregunta
     *
     * @param \AppBundle\Entity\Preguntes $idPregunta
     *
     * @return Respostes
     */
    public function setIdPregunta(\AppBundle\Entity\Preguntes $idPregunta = null)
    {
        $this->idPregunta = $idPregunta;

        return $this;
    }

    /**
     * Get idPregunta
     *
     * @return \AppBundle\Entity\Preguntes
     */
    public function getIdPregunta()
    {
        return $this->idPregunta;
    }
    
/*    
    public function __construct($idPregunta)
    {
        $this->idPregunta = $idPregunta;
    }
*/
    public function __construct() {}
}
