<?php

namespace AppBundle\Entity;

class PreguntesRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM AppBundle:Preguntes p ORDER BY p.text_pregunta ASC'
            )
            ->getResult();
    }

}
