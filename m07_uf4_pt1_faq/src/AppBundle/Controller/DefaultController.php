<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Preguntes;
use AppBundle\Entity\Respostes;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('index/index.html.twig', [
        ]);
    }
    
    
    /**
     * @Route("/inserir", name="inserir")
     */
    public function insertAction(Request $request)
    {
        return $this->render('preguntes/form.html.twig', array(
        ));
    }
    
    
    /**
     * @Route("/inserirNovaPregunta", name="inserirNovaPregunta")
     */
    public function insertNewQuestionAction(Request $request)
    {

		// Comprovem si s'arriba a aquesta URL a través del formulari correctament enviat
    	if ( $_POST && isset($_POST['question']) ){

			// DADES
    		$q = $_POST['question'];
    		$a1 = $_POST['answer1'];
    		$a2 = $_POST['answer2'];
    		$a3 = $_POST['answer3'];
    		$a4 = $_POST['answer4'];
    		$correct = $_POST['correct'];
    		
    		// Creem l'objecte [Preguntes]
    		$questionObj = new Preguntes();
    		$questionObj->setTextPregunta($q);
    		
    		// El guardem a la BD
    		$em = $this->getDoctrine()->getManager();
    		$em->persist($questionObj);
    		$em->flush();
    		
			// Aquí extreiem l'ID del nou objecte [Preguntes] atribuït per Doctrine
			// així ja sabem quin ID atribuir a les respostes en el camp [idPregunta]
    		$questionObjID = $questionObj->getId();
    		
    		// Creem els objectes [Respostes]
    		$answerObj1 = new Respostes(); 
    		$answerObj2 = new Respostes(); 
    		$answerObj3 = new Respostes(); 
    		$answerObj4 = new Respostes();
    		
    		// IDs
    		$answerObj1->setIdPregunta($questionObj);
    		$answerObj2->setIdPregunta($questionObj);
    		$answerObj3->setIdPregunta($questionObj);
    		$answerObj4->setIdPregunta($questionObj);
    		    		
    		// El text de cada resposta
    		$answerObj1->setTextResposta($a1);
    		$answerObj2->setTextResposta($a2);
    		$answerObj3->setTextResposta($a3);
    		$answerObj4->setTextResposta($a4);
    		
    		// I quina d'elles és la correcta
   	 	if ($correct == 1){
   	 		$answerObj1->setCorrecta(1);
   	 		$answerObj2->setCorrecta(0);
				$answerObj3->setCorrecta(0);
				$answerObj4->setCorrecta(0);
   	 	} else if ($correct == 2){
   	 		$answerObj1->setCorrecta(0);
   	 		$answerObj2->setCorrecta(1);
				$answerObj3->setCorrecta(0);
				$answerObj4->setCorrecta(0);
   	 	} else if ($correct == 3){
   	 		$answerObj1->setCorrecta(0);
   	 		$answerObj2->setCorrecta(0);
				$answerObj3->setCorrecta(1);
				$answerObj4->setCorrecta(0);
   	 	} else if ($correct == 4){
   	 		$answerObj1->setCorrecta(0);
   	 		$answerObj2->setCorrecta(0);
				$answerObj3->setCorrecta(0);
				$answerObj4->setCorrecta(1);
   	 	} 
   	 	
   	 	// Guardem a la BD les respostes
   	 	$em = $this->getDoctrine()->getManager();
   	 	$em->persist($answerObj1);
   	 	$em->persist($answerObj2);
   	 	$em->persist($answerObj3);
   	 	$em->persist($answerObj4);
   	 	$em->flush();
   	 					 		
    			
        return $this->render('default/message.html.twig', array(
				'message' => 'Insert successfully'));
        
      } else {
			return $this->render('default/message.html.twig', array(
				'message' => 'You can\'t use this route without using Insert Form'));
      }
    }
    
    
    /**
     * @Route("/esborrar", name="esborrar")
     */
    public function deleteAction(Request $request)
    {
    	
    	$em = $this->getDoctrine()->getManager();
    	
    	// Eliminem totes les respostes
   	$RAW_DELETE_QUERY = 'DELETE FROM respostes';
		$statement = $em->getConnection()->prepare($RAW_DELETE_QUERY);
		$statement->execute();
		
		// Eliminem totes les preguntes després per temes de clau forànies
		$RAW_DELETE_QUERY = 'DELETE FROM preguntes';
		$statement = $em->getConnection()->prepare($RAW_DELETE_QUERY);
		$statement->execute();
		
		return $this->render('default/message.html.twig', array(
			'message' => 'All answers ad question has been deleted'));
    }
    
    /**
     * @Route("/preguntar", name="preguntar")
     */
    public function askAction(Request $request)
    {
    		
   		// Obtenim el nombre de preguntes
    		$repo = $this->getDoctrine()
    			->getManager()
    			->getRepository('AppBundle:Preguntes');
    		
			$qb = $repo->createQueryBuilder('id');
			$qb->select('COUNT(id)');

			$count = $qb->getQuery()->getSingleScalarResult();
    	
        return $this->render('preguntes/ask.html.twig', array(
        		'numPreguntesDisponibles' => $count
        ));
    }
    
    /**
     * @Route("/consumirPreguntaForm", name="consumirPreguntaForm")
     */
    public function consumirPreguntaFormAction(Request $request)
    {
    	
    	if ( $_POST && isset($_POST['posicioPregunta']) ){
    		
    		$em = $this->getDoctrine()->getManager();
    		$posicio = $_POST['posicioPregunta'];
    		
    		//SQL a la taula Preguntes
	   	$RAW_SELECT_PREGUNTES_QUERY = 'SELECT * FROM preguntes LIMIT ' . (intval($posicio)-1) . ', 1';
			$statement = $em->getConnection()->prepare($RAW_SELECT_PREGUNTES_QUERY);
			$statement->execute();
			$results = $statement->fetchAll();
			
			// Ens quedem amb l'ID de la pregunta i el text
			$idPregunta = $results[0]['id'];
			$textPregunta = $results[0]['text_pregunta'];
			
    		//SQL a la taula Respostes
	   	$RAW_SELECT_RESPOSTES_QUERY = 'SELECT * FROM respostes WHERE id_pregunta = ' . $idPregunta;
			$statement = $em->getConnection()->prepare($RAW_SELECT_RESPOSTES_QUERY);
			$statement->execute();
			$results = $statement->fetchAll();
			
			// Agafem les respostes i les seves dades
			$textResposta1 = $results[0]['text_resposta'];
			$textResposta2 = $results[1]['text_resposta'];
			$textResposta3 = $results[2]['text_resposta'];
			$textResposta4 = $results[3]['text_resposta'];
			
			$numRespostes = 4;
			for($i=0; $i<$numRespostes; $i++){
				if ( $results[$i]['correcta'] )
					$respostaCorrecta = $i+1;
			}
			
        return $this->render('preguntes/consumirPreguntaForm.html.twig', array(
        		'question' 	=> $textPregunta,
        		'answer1' 	=> $textResposta1,
        		'answer2' 	=> $textResposta2,
        		'answer3' 	=> $textResposta3,
        		'answer4' 	=> $textResposta4,
				'respostaCorrecta' => $respostaCorrecta
        ));
        
       } else {
			return $this->render('default/message.html.twig', array(
				'message' => 'You can\'t use this route without using Dice'));
      }
      
    }

}
